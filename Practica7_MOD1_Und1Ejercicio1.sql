DROP DATABASE IF EXISTS practica7mod1; 
CREATE DATABASE practica7mod1;
USE practica7mod1;

CREATE TABLE alumno (
mat int,
nombre varchar(100),
grupo varchar(100),
  PRIMARY KEY (mat)
);

CREATE TABLE realiza (
matAlumno int,
`p#Practica`varchar(100),
  PRIMARY KEY (matAlumno,`p#Practica`)
);

CREATE TABLE practica (
`P#` varchar(100),
titulo varchar(100),
dificultad varchar(100),
  PRIMARY KEY (`P#`)
);

CREATE TABLE fechaP (
  `mat-Alumno` int,
  `p#Practica` varchar(100),
  nota float,
  fecha date,
  PRIMARY KEY (`mat-Alumno`,`p#Practica`) 
);

ALTER TABLE realiza
  ADD CONSTRAINT fkrealizaAlumno FOREIGN KEY (matAlumno) REFERENCES alumno (mat) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkrealizaPractica FOREIGN KEY (`p#Practica`) REFERENCES practica (`P#`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE fechaP
  ADD CONSTRAINT fkfechaPRealiza FOREIGN KEY (`mat-Alumno`, `p#Practica`) REFERENCES realiza (matAlumno, `p#Practica`) ON DELETE RESTRICT ON UPDATE RESTRICT;  ADD CONSTRAINT fkfechaPRealiza1 FOREIGN KEY (`p#Practica`) REFERENCES realiza (`p#Practica`) ON DELETE RESTRICT ON UPDATE RESTRICT;
   


  