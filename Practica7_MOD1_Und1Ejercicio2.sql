USE practica7mod1;

INSERT INTO alumno (mat, nombre, grupo) VALUES
                     (10, 'Pedro Casitas', '3A'),
                     (11,  'Juan Mazas',   '6B')
;

SELECT * FROM alumno a;

INSERT INTO practica 
  (`P#`, titulo, dificultad) VALUES
   ('SQL', 'Base de Datos', 'Media'),
   ('CMS', 'Desarrollador', 'Media');

SELECT * FROM practica p;

INSERT INTO realiza 
  (matAlumno, `p#Practica`) VALUES
  (10,         'SQL'),
  (11,         'CMS');

SELECT * FROM realiza r;



INSERT INTO fechap 
  (`mat-Alumno`, `p#Practica`, nota, fecha) VALUES 
  ('10',         'SQL',         10, '2022/09/10'),
  ('11',          'CMS',         8, '2021/10/12');

SELECT * FROM fechap f;




 